const { expect } = require('chai');
const { ethers } = require('hardhat')


describe("Counter", () => {
    let counter

    beforeEach(async () => {
        const Counter = await ethers.getContractFactory("Counter")
        counter = await Counter.deploy("Test Counter", 1)
    })

    describe("Deployment", () => {
        it("Stores inital name", async () => {
            const name = await counter.name()
            expect(name).to.equal("Test Counter")
        })
    
        it("Stores inital count", async () => {
            const count = await counter.count()
            expect(count).to.equal(1)
        })
    })

    describe("Counting", () => {
        let transaction

        it("reads from count public variable", async() => {
            expect(await counter.count()).to.equal(1)
        })

        it("reads from getCount() function", async() => {
            expect(await counter.getCount()).to.equal(1)
        })

        it("increase amount", async () => {
            transaction = await counter.increment()
            await transaction.wait()
            expect(await counter.count()).to.equal(2)

            transaction = await counter.increment()
            await transaction.wait()
            expect(await counter.count()).to.equal(3)
        })
    
        it("decrease amount", async () => {
            transaction = await counter.decrement()
            await transaction.wait()
            expect(await counter.count()).to.equal(0)
            await expect( counter.decrement()).to.be.reverted
        })

        it("reads from name public variable", async() => {
            expect(await counter.name()).to.equal("Test Counter")
        })

        it("reads from getName() function", async() => {
            expect(await counter.getName()).to.equal("Test Counter")
        })

        it("updates name", async() => {
            transaction = await counter.setName("New Name")
            await transaction.wait()
            expect(await counter.name()).to.equal("New Name")
        })
    })
})