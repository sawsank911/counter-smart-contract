// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract Counter {
    string public name = "Sawsank Counter";
    uint public count = 1;

    constructor(string memory _name, uint _initialCount){
        name = _name;
        count = _initialCount;
    }

    function increment() public returns (uint newCount) {
        count++;
        return count;
    }

    function decrement() public returns (uint newCount){
        count--;
        return count;
    }

    function getCount() public view returns (uint){
        return count;
    }

    function getName() public view returns (string memory){
        return name;
    }

    function setName(string memory _newName) public returns (string memory){
        name = _newName;
        return name;
    }
}